// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HAL/ThreadSafeBool.h"
#include "ThreadExample_hwGameModeBase.h"

//class ThreadExample_hwGameModeBase

class THREADEXAMPLE_HW_API SimpleMutexAge_runnable : public FRunnable
{
public:
	SimpleMutexAge_runnable(AThreadExample_hwGameModeBase* Owner, int min, int max);
	~SimpleMutexAge_runnable();

	virtual uint32 Run() override;
	virtual void Stop() override;
	virtual void Exit() override;

	int AgeMin = 0;
	int AgeMax = 0;


	AThreadExample_hwGameModeBase* refGameMode = nullptr;
	FThreadSafeBool bIsStopGenerate = false;

	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> SenderGeneratorEndpoint;
};
