// Fill out your copyright notice in the Description page of Project Settings.

#include "SimpleCollectable_runnable.h"
#include "ThreadExample_hwGameModeBase.h"

SimpleCollectable_runnable::SimpleCollectable_runnable(AThreadExample_hwGameModeBase* Owner)
{
    refGameMode = Owner;

    SenderCollectorEndpoint = FMessageEndpoint::Builder("Collector_Sender").Build();
}

SimpleCollectable_runnable::~SimpleCollectable_runnable()
{
}

uint32 SimpleCollectable_runnable::Run()
{
    int32 i = 0;

    while (!bIsStopCollectable)
    {
        int32 sizeAges = refGameMode->all_age.Num();
        //int32 sizeColors = refGameMode->all_color.Num();

        TArray<FLinearColor> AvaibleAllColors = refGameMode->GetCubeColors();
        int32 sizeAllColors = AvaibleAllColors.Num();

        //if (sizeAges > 0 && sizeColors > 0)
        if (sizeAges > 0 && sizeAllColors > 0)
        {
            FCube_info newCube;
            newCube.Cube_id = i;
            i++;

            refGameMode->CubeAgesMutex.Lock();
            newCube.Cube_age = refGameMode->all_age[0];
            refGameMode->all_age.RemoveAt(0);
            refGameMode->CubeAgesMutex.Unlock();
        
            //refGameMode->CubeColorMutex.Lock();
            //newCube.Cube_color = refGameMode->all_color[0];
            //refGameMode->all_color.RemoveAt(0);
            //refGameMode->CubeColorMutex.Unlock();

            newCube.Cube_color = AvaibleAllColors[0];
            refGameMode->conv_colors.RemoveAt(0);

            {
                //FScopeLock CubesScopedLock(&refGameMode->CubesInfoMutex);
                refGameMode->Cubes_info.Add(newCube);
            }

            if (SenderCollectorEndpoint.IsValid())
            {
                SenderCollectorEndpoint->Publish<FCube_info>(new FCube_info(newCube));
            }
        }
    }

    return uint32();
}

void SimpleCollectable_runnable::Stop()
{
    bIsStopCollectable = true;
}

void SimpleCollectable_runnable::Exit()
{
    if (SenderCollectorEndpoint.IsValid())
    {
        SenderCollectorEndpoint.Reset();
    }
    refGameMode = nullptr;
}
