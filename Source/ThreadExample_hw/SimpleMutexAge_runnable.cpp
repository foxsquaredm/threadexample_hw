// Fill out your copyright notice in the Description page of Project Settings.


#include "SimpleMutexAge_runnable.h"

SimpleMutexAge_runnable::SimpleMutexAge_runnable(AThreadExample_hwGameModeBase* Owner, int min, int max)
{
	refGameMode = Owner;
	AgeMin = min;
	AgeMax = max;

	SenderGeneratorEndpoint = FMessageEndpoint::Builder("GeneratorAge_Sender").Build();
}

SimpleMutexAge_runnable::~SimpleMutexAge_runnable()
{
}

uint32 SimpleMutexAge_runnable::Run()
{
	while (!bIsStopGenerate)
	{
		int32 curAge = 0;

		curAge = refGameMode->GetRandomInt(AgeMin, AgeMax);
		refGameMode->CubeAgesMutex.Lock();
		refGameMode->all_age.Add(curAge);
		refGameMode->CubeAgesMutex.Unlock();


		if (SenderGeneratorEndpoint.IsValid())
		{
			SenderGeneratorEndpoint->Publish<FStructMessage_descCube>(new FStructMessage_descCube(curAge));
		}
		FPlatformProcess::Sleep(0.5f);
	}

	return 1;
}

void SimpleMutexAge_runnable::Stop()
{
	bIsStopGenerate = true;
}

void SimpleMutexAge_runnable::Exit()
{
	if (SenderGeneratorEndpoint.IsValid())
	{
		SenderGeneratorEndpoint.Reset();
	}
	refGameMode = nullptr;
}
