// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ThreadExample_hwGameModeBase.h"
#include "Cube_Generated.generated.h"

UCLASS()
class THREADEXAMPLE_HW_API ACube_Generated : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACube_Generated();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	//// Called every frame
	//virtual void Tick(float DeltaTime) override;

	void Init(FCube_info InitInfo);

	UFUNCTION(BlueprintNativeEvent)
		void InitBP(FCube_info InitInfo);

};
