// Fill out your copyright notice in the Description page of Project Settings.


#include "SimpleMutexColor_runnable.h"

SimpleMutexColor_runnable::SimpleMutexColor_runnable(AThreadExample_hwGameModeBase* Owner, int max)
{
	refGameMode = Owner;
	ColorMax = max;

	SenderGeneratorEndpoint = FMessageEndpoint::Builder("GeneratorColor_Sender").Build();
}

SimpleMutexColor_runnable::~SimpleMutexColor_runnable()
{
}

uint32 SimpleMutexColor_runnable::Run()
{
	while (!bIsStopGenerate)
	{
		FLinearColor curColor = FLinearColor::Black;

		int32 ColorMax = refGameMode->Cube_ColorMax;
		//curColor = refGameMode->GetRandomInt(0, ColorMax);
		float R = refGameMode->GetRandomInt(0, ColorMax) / 1000.f;
		float G = refGameMode->GetRandomInt(0, ColorMax) / 1000.f;
		float B = refGameMode->GetRandomInt(0, ColorMax) / 1000.f;
		curColor = FLinearColor::FLinearColor(R, G, B, 1.f);
		refGameMode->cube_colors.Enqueue(curColor);

		//refGameMode->CubeColorMutex.Lock();
		//refGameMode->all_color.Add(curColor);
		//refGameMode->CubeColorMutex.Unlock();

		if (SenderGeneratorEndpoint.IsValid())
		{
			SenderGeneratorEndpoint->Publish<FStructMessage_descCube>(new FStructMessage_descCube(0, curColor));
		}
		FPlatformProcess::Sleep(0.5f);
	}

	return 1;
}

void SimpleMutexColor_runnable::Stop()
{
	bIsStopGenerate = true;
}

void SimpleMutexColor_runnable::Exit()
{
	if (SenderGeneratorEndpoint.IsValid())
	{
		SenderGeneratorEndpoint.Reset();
	}
	refGameMode = nullptr;
}
