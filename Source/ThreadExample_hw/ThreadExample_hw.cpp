// Copyright Epic Games, Inc. All Rights Reserved.

#include "ThreadExample_hw.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ThreadExample_hw, "ThreadExample_hw" );
