// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HAL/ThreadSafeBool.h"
#include "ThreadExample_hwGameModeBase.h"


class THREADEXAMPLE_HW_API SimpleMutexColor_runnable : public FRunnable
{
public:
	SimpleMutexColor_runnable(AThreadExample_hwGameModeBase* Owner, int max);
	~SimpleMutexColor_runnable();

	virtual uint32 Run() override;
	virtual void Stop() override;
	virtual void Exit() override;

	int ColorMax = 0;


	AThreadExample_hwGameModeBase* refGameMode = nullptr;
	FThreadSafeBool bIsStopGenerate = false;

	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> SenderGeneratorEndpoint;
};
