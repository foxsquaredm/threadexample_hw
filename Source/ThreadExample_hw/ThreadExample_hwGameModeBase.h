// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
//#include "Math/Color.h"
#include "SimpleCollectable_Runnable.h"
#include "MessageEndpoint.h"
#include "MessageEndpointBuilder.h"
#include "GameFramework/GameModeBase.h"
#include "ThreadExample_hwGameModeBase.generated.h"


USTRUCT(BlueprintType, Atomic)
struct FCube_info
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, Category = "Cube description")
		int32 Cube_id;
	//1. ������������ ������� ������ ����� ��������������� UE4. �������� ����� � ������� ���.
	UPROPERTY(BlueprintReadWrite, Category = "Cube description")
		int32 Cube_age;
	//2. ������������ ���� ������ ����� ��������������� UE4.�������� ����� � ������� ���.���� ������ ������������ �� ��������� ��� ������.
	UPROPERTY(BlueprintReadWrite, Category = "Cube description")
		FLinearColor Cube_color;

};

USTRUCT()
struct FStructMessage_descCube
{
	GENERATED_USTRUCT_BODY()

	int32 Age = 0;
	//FColor Color = FColor::White;
	FLinearColor Color = FLinearColor::Black;

	//FStructMessageAge(int32 InIntAge = 0) : Age(InIntAge) {};
	FStructMessage_descCube(int32 InIntAge = 0, FLinearColor InColor = FLinearColor::Black) : Age(InIntAge), Color(InColor) {};
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdate_ByGeneratorAge, int32, curAge);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdate_ByGeneratorColor, FLinearColor, curColor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdate_ByCubeThread, FCube_info, NewCubeInfo);

UCLASS()
class THREADEXAMPLE_HW_API AThreadExample_hwGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cube")
		TSubclassOf<class ACube_Generated> ThreadCubeClass;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cube")
		int32 Cube_AgeMin = 5;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cube")
		int32 Cube_AgeMax = 50;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cube")
		int32 Cube_ColorMax = 255;


	UPROPERTY(BlueprintAssignable)
		FOnUpdate_ByGeneratorAge OnUpdate_ByGeneratorAge;
	UPROPERTY(BlueprintAssignable)
		FOnUpdate_ByGeneratorColor OnUpdate_ByGeneratorColor;
	UPROPERTY(BlueprintAssignable)
		FOnUpdate_ByCubeThread OnUpdate_ByCubeThread;

	TArray<FRunnableThread*> curRunningThread_SimpleMutex;
	FRunnableThread* curRunningThread_SimpleCollectable = nullptr;

	TArray<FCube_info> Cubes_info;
	FCriticalSection CubeInfoMutex;

	TArray<int32> all_age;
	FCriticalSection CubeAgesMutex;

	//TArray<FLinearColor> all_color;
	FCriticalSection CubeColorMutex;

	TQueue<int32, EQueueMode::Mpsc> cube_ages;
	TQueue<FLinearColor, EQueueMode::Mpsc> cube_colors;

	TArray<int32> conv_ages;
	TArray<FLinearColor> conv_colors;


	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> ReciveEndpoint_GeneratorAge;
	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> ReciveEndpoint_GeneratorColor;
	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> ReciveEndpoint_CubeInfo;

	void BusMessageHandler_GenetatorDescCube(const struct FStructMessage_descCube& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);
	void BusMessageHandler_GenetatorColor(const struct FStructMessage_descCube& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);
	void BusMessageHandler_CubeInfo(const struct FCube_info& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context);

	UFUNCTION(BlueprintCallable, Category = "ThreadTest")
		void Create_SimpleMutexThread_Age();
	UFUNCTION(BlueprintCallable, Category = "ThreadTest")
		void Create_SimpleMutexThread_Color(); 
	UFUNCTION(BlueprintCallable, Category = "ThreadTest")
		void Create_SimpleCollectableThread();
	UFUNCTION(BlueprintCallable, Category = "ThreadTest")
		void Stop_SimpleMutexThreads();

	UFUNCTION(BlueprintCallable, Category = "ThreadTest")
		int32 GetRandomInt(int32 min, int32 max);
	UFUNCTION(BlueprintCallable, Category = "ThreadTest")
		TArray<int32> GetCubeAges();
	UFUNCTION(BlueprintCallable, Category = "ThreadTest")
		TArray<FLinearColor> GetCubeColors();

	void EventMessage_GeneratorAge(int32 Age);
	void EventMessage_GeneratorColor(FLinearColor Color);
	void EventMessage_CubeInfo(FCube_info CubeData);

	void DEstoryAllCubes();
};
