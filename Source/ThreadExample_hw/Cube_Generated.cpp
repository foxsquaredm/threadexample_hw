// Fill out your copyright notice in the Description page of Project Settings.

//#include "ThreadExample_hwGameModeBase.h"
#include "Cube_Generated.h"

// Sets default values
ACube_Generated::ACube_Generated()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACube_Generated::BeginPlay()
{
	Super::BeginPlay();
	
}

//// Called every frame
//void ACube_Generated::Tick(float DeltaTime)
//{
//	Super::Tick(DeltaTime);
//
//}

void ACube_Generated::Init(FCube_info InitInfo)
{
	InitBP(InitInfo);
}

void ACube_Generated::InitBP_Implementation(FCube_info InitInfo)
{
	//for use in BP
}

