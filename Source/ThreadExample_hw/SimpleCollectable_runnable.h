// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MessageEndpoint.h"
#include "ThreadExample_hwGameModeBase.h"

class AThreadExample_hwGameModeBase;


class THREADEXAMPLE_HW_API SimpleCollectable_runnable : public FRunnable
{
public:
	SimpleCollectable_runnable(AThreadExample_hwGameModeBase* Owner);
	virtual ~SimpleCollectable_runnable() override;

	virtual uint32 Run() override;
	virtual void Stop() override;
	virtual void Exit() override;


	AThreadExample_hwGameModeBase* refGameMode = nullptr;
	FThreadSafeBool bIsStopCollectable = false;

	TSharedPtr<FMessageEndpoint, ESPMode::ThreadSafe> SenderCollectorEndpoint;
};
