// Copyright Epic Games, Inc. All Rights Reserved.

#include "ThreadExample_hwGameModeBase.h"
#include <random>
#include "SimpleMutexAge_runnable.h"
#include "SimpleMutexColor_runnable.h"
#include "Cube_Generated.h"
#include "Kismet/GameplayStatics.h"

void AThreadExample_hwGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	ReciveEndpoint_GeneratorAge = FMessageEndpoint::Builder("Reciever_GeneratorAge").Handling<FStructMessage_descCube>(this, &AThreadExample_hwGameModeBase::BusMessageHandler_GenetatorDescCube);
	ReciveEndpoint_GeneratorColor = FMessageEndpoint::Builder("Reciever_GeneratorColor").Handling<FStructMessage_descCube>(this, &AThreadExample_hwGameModeBase::BusMessageHandler_GenetatorDescCube);
	ReciveEndpoint_CubeInfo = FMessageEndpoint::Builder("Reciever_CubeInfo").Handling<FCube_info>(this, &AThreadExample_hwGameModeBase::BusMessageHandler_CubeInfo);

	if (ReciveEndpoint_GeneratorAge.IsValid())
	{
		ReciveEndpoint_GeneratorAge->Subscribe<FStructMessage_descCube>();
	}
	if (ReciveEndpoint_GeneratorColor.IsValid())
	{
		ReciveEndpoint_GeneratorColor->Subscribe<FStructMessage_descCube>();
	}

	if (ReciveEndpoint_CubeInfo.IsValid())
	{
		ReciveEndpoint_CubeInfo->Subscribe<FCube_info>();
	}
}

void AThreadExample_hwGameModeBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Stop_SimpleMutexThreads();

	if (ReciveEndpoint_GeneratorAge.IsValid())
	{
		ReciveEndpoint_GeneratorAge.Reset();
	}
	if (ReciveEndpoint_GeneratorColor.IsValid())
	{
		ReciveEndpoint_GeneratorColor.Reset();
	}

	if (ReciveEndpoint_CubeInfo.IsValid())
	{
		ReciveEndpoint_CubeInfo.Reset();
	}

	Super::EndPlay(EndPlayReason);
}

void AThreadExample_hwGameModeBase::BusMessageHandler_GenetatorDescCube(const FStructMessage_descCube& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context)
{
	EventMessage_GeneratorAge(Message.Age);
	//EventMessage_GeneratorColor(Message.Color);
}

void AThreadExample_hwGameModeBase::BusMessageHandler_GenetatorColor(const FStructMessage_descCube& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context)
{
	EventMessage_GeneratorColor(Message.Color);
}

void AThreadExample_hwGameModeBase::BusMessageHandler_CubeInfo(const FCube_info& Message, const TSharedRef<IMessageContext, ESPMode::ThreadSafe>& Context)
{
	EventMessage_CubeInfo(Message);
}

void AThreadExample_hwGameModeBase::Create_SimpleMutexThread_Age()
{
	SimpleMutexAge_runnable *stAge = new SimpleMutexAge_runnable(this, Cube_AgeMin, Cube_AgeMax);

	curRunningThread_SimpleMutex.Add(FRunnableThread::Create(stAge, TEXT("SimpleMutex Age Thread"), 0, EThreadPriority::TPri_Lowest));
}

void AThreadExample_hwGameModeBase::Create_SimpleMutexThread_Color()
{
	SimpleMutexColor_runnable* stColor = new SimpleMutexColor_runnable(this, Cube_ColorMax);

	curRunningThread_SimpleMutex.Add(FRunnableThread::Create(stColor, TEXT("SimpleMutex Color Thread"), 0, EThreadPriority::TPri_Lowest));
}

void AThreadExample_hwGameModeBase::Create_SimpleCollectableThread()
{
	DEstoryAllCubes();

	SimpleCollectable_runnable* MySimpleCollectable = new SimpleCollectable_runnable(this);

	curRunningThread_SimpleCollectable = FRunnableThread::Create(MySimpleCollectable, TEXT("SimpleCollectable Thread"), 0, EThreadPriority::TPri_Lowest);
}

void AThreadExample_hwGameModeBase::Stop_SimpleMutexThreads()
{
	if (curRunningThread_SimpleMutex.Num() > 0)
	{
		for (auto RunnableThread : curRunningThread_SimpleMutex)
		{
			if (RunnableThread)
			{
				RunnableThread->Kill(true);
			}
		}
		curRunningThread_SimpleMutex.Empty();
	}

	if (curRunningThread_SimpleCollectable)
	{
		curRunningThread_SimpleCollectable->Kill(true);
		curRunningThread_SimpleCollectable = nullptr;
	}
}

int32 AThreadExample_hwGameModeBase::GetRandomInt(int32 min, int32 max)
{

	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> distrib(min, max);

	return distrib(gen);
}

TArray<int32> AThreadExample_hwGameModeBase::GetCubeAges()
{
	return TArray<int32>();
}

TArray<FLinearColor> AThreadExample_hwGameModeBase::GetCubeColors()
{
	TArray<FLinearColor> result;
	FLinearColor Color;
	while (cube_colors.Dequeue(Color))
	{
		result.Add(Color);
	}
	conv_colors.Append(result);

	return conv_colors;
}

void AThreadExample_hwGameModeBase::EventMessage_GeneratorAge(int32 Age)
{
	OnUpdate_ByGeneratorAge.Broadcast(Age);
}

void AThreadExample_hwGameModeBase::EventMessage_GeneratorColor(FLinearColor Color)
{
	OnUpdate_ByGeneratorColor.Broadcast(Color);
}

void AThreadExample_hwGameModeBase::EventMessage_CubeInfo(FCube_info CubeData)
{
	OnUpdate_ByCubeThread.Broadcast(CubeData);

	const auto World = GetWorld();
	if (World && ThreadCubeClass)
	{
		const int32 row_count = 19;
		int32 z_shift = (CubeData.Cube_id - (CubeData.Cube_id % row_count)) / row_count;

		FVector SpawnLoc = FVector(900.f, (100.f * (CubeData.Cube_id % row_count)) - 900.f, 100.f * z_shift + 100.f);
		FRotator SpawnRot = FRotator(0);

		auto myCube = World->SpawnActor<ACube_Generated>(ThreadCubeClass, SpawnLoc, SpawnRot, FActorSpawnParameters());
		if (myCube)
		{
			myCube->Init(CubeData);
		}
	}
}

void AThreadExample_hwGameModeBase::DEstoryAllCubes()
{
	TArray<AActor*> CubesForDestroy;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ThreadCubeClass, CubesForDestroy);

	if (CubesForDestroy.Num() > 0)
	{
		for (auto curCube : CubesForDestroy) 
		{
			if (curCube)
			curCube->Destroy();
		}
	}

}
